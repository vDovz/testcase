﻿namespace DataAccess.Interfaces
{
    public interface IBaseDataProvider
    {
        string Get();
    }
}
