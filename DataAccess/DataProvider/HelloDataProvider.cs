﻿using DataAccess.Interfaces;

namespace DataAccess.DataProvider
{
    public class HelloDataProvider : IHelloDataProvider
    {
        public string Get()
        {
            return "Hi everyone!";
        }
    }
}
