﻿using DataAccess.Interfaces;

namespace DataAccess.DataProvider
{
    public class HiDataProvider : IHiDataProvider
    {
        public string Get()
        {
            return "Hi there!";
        }
    }
}
