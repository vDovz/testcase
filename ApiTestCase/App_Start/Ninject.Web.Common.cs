[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(ApiTestCase.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(ApiTestCase.App_Start.NinjectWebCommon), "Stop")]

namespace ApiTestCase.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.Common.WebHost;
    using BusinessLogic.Interfaces;
    using BusinessLogic.Services;
    using ApiTestCase.Controllers;
    using DataAccess.Interfaces;
    using DataAccess.DataProvider;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IHiDataProvider>().To<HiDataProvider>();
            kernel.Bind<IHelloDataProvider>().To<HelloDataProvider>();

            kernel.Bind<IGreeter>().To<HiService>().WhenInjectedInto(typeof(HiController)).WithConstructorArgument("provider", x => x.Kernel.Get<IHiDataProvider>());
            kernel.Bind<IGreeter>().To<HelloService>().WhenInjectedInto(typeof(HelloController)).WithConstructorArgument("provider", x => x.Kernel.Get<IHelloDataProvider>());
        }        
    }
}