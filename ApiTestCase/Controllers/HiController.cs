﻿using BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace ApiTestCase.Controllers
{
    public class HiController : ApiController
    {
        private readonly IGreeter _greeter;

        public HiController (IGreeter greeter)
        {
            _greeter = greeter;
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            string result = _greeter.SayHello();
            return Ok(result);
        }
    }
}
