﻿using BusinessLogic.Interfaces;
using System.Web.Http;

namespace ApiTestCase.Controllers
{
    public class HelloController : ApiController
    {
        private readonly IGreeter _greeter;

        public HelloController(IGreeter greeter) 
        {
            _greeter = greeter;
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            string result = _greeter.SayHello();
            return Ok(result);
        }
    }
}
