﻿namespace BusinessLogic.Interfaces
{
    public interface IGreeter
    {
        string SayHello();
    }

}
