﻿using BusinessLogic.Interfaces;
using DataAccess.Interfaces;

namespace BusinessLogic.Services
{
    public class HiService : IGreeter
    {
        private readonly IHiDataProvider _provider;

        public HiService(IHiDataProvider provider)
        {
            _provider = provider;
        }

        public string SayHello()
        {
            return _provider.Get();
        }
    }
}
