﻿using BusinessLogic.Interfaces;
using DataAccess.Interfaces;

namespace BusinessLogic.Services
{
    public class HelloService : IGreeter
    {
        private readonly IHelloDataProvider _provider;

        public HelloService(IHelloDataProvider provider)
        {
            _provider = provider;
        }

        public string SayHello()
        {
            return _provider.Get();
        }
    }
}
